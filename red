#!/usr/bin/env bash
echo "<html><body><link rel='stylesheet' type='text/css' href='./style.css'>" > tmp.html
pbpaste | awk -F "." '/./{  print "<p>"$1".</p>" }' >> tmp.html
echo "</body></html>" >> tmp.html
open -a safari tmp.html
